package trie;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TrieNodeTest {
    @Test
    public void addWord_wordInTrie() {
        TrieNode root = new TrieNode();
        TrieNode.addWord("HELLO", 0, root);
        var expectedTrieRoot = new TrieNode();
        expectedTrieRoot.children['H' - 'A'] = new TrieNode();
        expectedTrieRoot.children['H' - 'A'].children['E' - 'A'] = new TrieNode();
        expectedTrieRoot.children['H' - 'A'].children['E' - 'A'].children['L' - 'A'] = new TrieNode();
        expectedTrieRoot.children['H' - 'A'].children['E' - 'A'].children['L' - 'A'].children['L' - 'A'] = new TrieNode();
        expectedTrieRoot.children['H' - 'A'].children['E' - 'A'].children['L' - 'A'].children['L' - 'A'].children['O' - 'A'] = new TrieNode();
        expectedTrieRoot.children['H' - 'A'].children['E' - 'A'].children['L' - 'A'].children['L' - 'A'].children['O' - 'A'].isWord = true;
        assertEquals(expectedTrieRoot, root);
    }

    @Test
    public void addTwoOverlappingWords_bothWordsInTrie() {
        TrieNode root = new TrieNode();
        TrieNode.addWord("HELLO", 0, root);
        TrieNode.addWord("HELP", 0, root);
        var expectedTrieRoot = new TrieNode();
        expectedTrieRoot.children['H' - 'A'] = new TrieNode();
        expectedTrieRoot.children['H' - 'A'].children['E' - 'A'] = new TrieNode();
        expectedTrieRoot.children['H' - 'A'].children['E' - 'A'].children['L' - 'A'] = new TrieNode();
        expectedTrieRoot.children['H' - 'A'].children['E' - 'A'].children['L' - 'A'].children['P' - 'A'] = new TrieNode();
        expectedTrieRoot.children['H' - 'A'].children['E' - 'A'].children['L' - 'A'].children['L' - 'A'] = new TrieNode();
        expectedTrieRoot.children['H' - 'A'].children['E' - 'A'].children['L' - 'A'].children['L' - 'A'].children['O' - 'A'] = new TrieNode();
        expectedTrieRoot.children['H' - 'A'].children['E' - 'A'].children['L' - 'A'].children['P' - 'A'].isWord = true;
        expectedTrieRoot.children['H' - 'A'].children['E' - 'A'].children['L' - 'A'].children['L' - 'A'].children['O' - 'A'].isWord = true;
        assertEquals(expectedTrieRoot, root);
    }
}
