import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class WordSearchSolverTest {
    @Test
    public void simpleBoard_solved() {
        char[][] board = {{'A', 'M'}, {'R', 'B'}};
        int numRows = 2;
        int numCols = 2;
        var words = List.of("AM", "BR");
        var wordSearchSolver = new WordSearchSolver(numRows, numCols, board, words);
        var solved = wordSearchSolver.solveBoard();
        assertEquals(List.of(WordSearchTuple.of("AM", 0, 0, 0, 1), WordSearchTuple.of("BR", 1, 1, 1, 0)), solved);
    }

    @Test
    public void sampleBoard_solved() {
        char[][] board = {{'H', 'A', 'S', 'D', 'F'}, {'G', 'E', 'Y', 'B', 'H'}, {'J', 'K', 'L', 'Z', 'X'}, {'C', 'V', 'B', 'L', 'N'}, {'G', 'O', 'O', 'D', 'O'}};
        int numRows = 5;
        int numCols = 5;
        var words = List.of("HELLO", "GOOD", "BYE");
        var wordSearchSolver = new WordSearchSolver(numRows, numCols, board, words);
        var solved = wordSearchSolver.solveBoard();
        assertEquals(List.of(WordSearchTuple.of("HELLO", 0, 0, 4, 4), WordSearchTuple.of("GOOD", 4, 0, 4, 3), WordSearchTuple.of("BYE", 1, 3, 1, 1)), solved);
    }


}

