import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class WordSearchSolverParserTest {
    @Test
    public void sampleFile_solved() throws IOException {
        var wordSearchSolver = WordSearchSolverParser.parse("src/main/resources/sample_input.txt");
        var solved = wordSearchSolver.solveBoard();
        assertEquals(List.of(WordSearchTuple.of("HELLO", 0, 0, 4, 4), WordSearchTuple.of("GOOD", 4, 0, 4, 3), WordSearchTuple.of("BYE", 1, 3, 1, 1)), solved);
    }
    @Test
    public void sampleFileMixedCase_solved() throws IOException {
        var wordSearchSolver = WordSearchSolverParser.parse("src/main/resources/sample_input_mixed_case.txt");
        var solved = wordSearchSolver.solveBoard();
        assertEquals(List.of(WordSearchTuple.of("HELLO", 0, 0, 4, 4), WordSearchTuple.of("GOOD", 4, 0, 4, 3), WordSearchTuple.of("BYE", 1, 3, 1, 1)), solved);
    }
}
