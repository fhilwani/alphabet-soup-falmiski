/** Record representing a succesful search pair from the word search puzzle. */
public record WordSearchTuple(String word, WordCoordinate begin, WordCoordinate end) {
public static WordSearchTuple of(String word, int si, int sj, int ei, int ej){
    return new WordSearchTuple(word, new WordCoordinate(si, sj), new WordCoordinate(ei, ej));
}
}
