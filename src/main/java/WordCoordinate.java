/** A record representing a coordinate in the board where either a start or end is for a word. */
public record WordCoordinate(int i, int j) {
}
