import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordSearchSolverParser {
    public static WordSearchSolver parse(String filename) throws IOException {
        var reader = new BufferedReader(new FileReader(filename));
        String dimensions = reader.readLine().toUpperCase();
        int numRows = Integer.parseInt(dimensions.substring(0, dimensions.indexOf('X')));
        int numCols = Integer.parseInt(dimensions.substring(dimensions.indexOf('X') + 1));
        char[][] board = new char[numRows][numRows];
        for (var i = 0; i < numRows; i++) {
            String[] characters = reader.readLine().split(" ");
            for (var j = 0; j < numCols; j++) {
                board[i][j] = characters[j].toUpperCase().charAt(0);
            }
        }

        List<String> words = new ArrayList<>();
        while (reader.ready()) {
            words.add(reader.readLine().toUpperCase());
        }
        return new WordSearchSolver(numRows, numCols, board, words);
    }
}
