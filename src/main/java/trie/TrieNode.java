package trie;

import java.util.Arrays;

/**
 * Node of a Trie. Used in solution of word search puzzle as efficient way of determining whether a stream of characters represents some word.
 */
public class TrieNode {
    public boolean isWord;
    public String word;
    public TrieNode[] children;

    public TrieNode() {
        children = new TrieNode[26];
    }

    public boolean contains(char c) {
        if (c < 'A' || c > 'Z') {
            return false;
        }
        int index = c - 'A';
        return children[index] != null;
    }

    public static void addWord(String word, int i, TrieNode node) {
        if (i == word.length()) {
            node.isWord = true;
            node.word = word;
            return;
        }
        var character = word.charAt(i);
        var index = character - 'A';
        if (!node.contains(character)) {
            node.children[index] = new TrieNode();
        }
        addWord(word, i + 1, node.children[index]);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TrieNode node) {
            return isWord == node.isWord && Arrays.equals(children, node.children);
        }
        return false;
    }
}
