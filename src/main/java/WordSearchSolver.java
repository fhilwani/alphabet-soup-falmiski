import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import trie.TrieNode;

/**
 * A class that solves word search puzzles given a board and list of words.
 */
public class WordSearchSolver {
    final int numRows;
    final int numCols;
    final char[][] board;
    final List<String> words;
    TrieNode root;
    final int[][] directions = {{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};


    public WordSearchSolver(int numRows, int numCols, char[][] board, List<String> words) {
        this.numRows = numRows;
        this.numCols = numCols;
        this.board = board;
        this.words = words;
        root = new TrieNode();
    }

    public List<WordSearchTuple> solveBoard() {
        // Read all words into a Trie to make it easier to solve.
        for (var word : words) {
            TrieNode.addWord(word, 0, root);
        }
        var wordSearchTupleMap = new HashMap<String, WordSearchTuple>();
        // Every position on the board could be the beginning of a word
        // so search from every position in all directions.
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                search(i, j, root, wordSearchTupleMap);
            }
        }
        var wordSearchTuples = new ArrayList<WordSearchTuple>();
        for (var word : words) {
            wordSearchTuples.add(wordSearchTupleMap.get(word));
        }
        return wordSearchTuples;
    }

    public void search(int i, int j, TrieNode currentNode, Map<String, WordSearchTuple> wordSearchTupleMap) {
        if (!validCoordinates(i, j)) {
            return;
        }
        char c = board[i][j];
        // Search the current position character within the root node,
        // if a word starts with that character continue in all directions attempting to match the characters with a word in the list of words.
        if (currentNode.contains(c)) {
            int index = c - 'A';
            var childNode = currentNode.children[index];
            if (childNode.isWord) {
                wordSearchTupleMap.put(childNode.word, WordSearchTuple.of(childNode.word, i, j, i, j));
            }
            for (var direction : directions) {
                var xDirection = direction[0];
                var yDirection = direction[1];
                var newI = i + xDirection;
                var newJ = j + yDirection;
                var newChildNode = childNode;
                while (validCoordinates(newI, newJ)) {
                    char newC = board[newI][newJ];
                    if (newChildNode.contains(newC)) {
                        newChildNode = newChildNode.children[newC - 'A'];
                        if (newChildNode.isWord) {
                            wordSearchTupleMap.put(newChildNode.word, WordSearchTuple.of(newChildNode.word, i, j, newI, newJ));
                        }
                    } else {
                        break;
                    }
                    newI += xDirection;
                    newJ += yDirection;
                }
            }
        }
    }

    /* Helper method to determine whether a pair of integers falls within the board. */
    private boolean validCoordinates(int i, int j) {
        return i >= 0 && i < numRows && j >= 0 && j < numCols;
    }
}
