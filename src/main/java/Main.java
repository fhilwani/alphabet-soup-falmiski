import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        if(args.length < 1){
            System.err.println("Must provide filename as argument.");
            return;
        }
        var filename = args[0];
        // Read in file from args and solve board puzzle.
        var wordSearchSolver = WordSearchSolverParser.parse(filename);
        var wordSearchTuples = wordSearchSolver.solveBoard();
        for(var coordinates : wordSearchTuples){
            System.out.printf("%s %d:%d %d:%d\n", coordinates.word(), coordinates.begin().i(), coordinates.begin().j(), coordinates.end().i(), coordinates.end().j());
        }
    }
}
